<?php
include 'header.php';

$act = 'add';

if (!empty($_GET['id_jabatan'])) {
    $sql = 'SELECT * FROM jabatan WHERE id_jabatan="' . $_GET['id_jabatan'] . '"';

    $query = mysqli_query($conn, $sql);

    if (mysqli_num_rows($query)) {
        $act = 'edit';

        $row = mysqli_fetch_object($query);
    }
}

if(!$_SESSION) {
	header('Location: formLogin.php');

}
?>
<h1>Form Jabatan</h1>
<div class="mb-3">
    <form method="post" action="saveJabatan.php">
        <div class="mb-3">
            <label for="form-label">Nama Jabatan</label>
            <input type="text" class="form-control" name="nama_jabatan" placeholder="Nama Jabatan" value="<?php if ($act == 'edit') echo $row->nama_jabatan; ?>" required>
            <input type="hidden" name="id_jabatan" value="<?php if ($act == 'edit') echo $row->id_jabatan; ?>">
        </div>

        </select>

        <div class="mb-3 ml-4">
            <input type="submit" class="btn btn-success" value="Simpan">
        </div>
    </form>

    <?php include 'footer.php'; ?>