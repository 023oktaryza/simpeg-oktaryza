<?php include 'header.php'; ?>
<h1>Pegawai</h1>
<a href="formPegawai.php" class="btn btn-info btn-sm mb-3 mt-3">Tambah</a>
<table class="table">
    <thead class="table-info">
        <tr>
            <th>No.</th>
            <th>Nama</th>
            <th>Jenis Kelamin</th>
            <th>Tanggal Lahir</th>
            <th>Alamat</th>
            <th>Jabatan</th>
			<?php if($_SESSION) : ?>
            <th>Aksi</th>
			<?php endif?>
        </tr>
    </thead>
    <tbody>

        <?php
        $sql = 'SELECT * FROM pegawai JOIN jabatan ON jabatan.id_jabatan = pegawai.id_jabatan';

        $query = mysqli_query($conn, $sql);

        $i = 1;

        while ($row = mysqli_fetch_object($query)) {
        ?>

            <tr>
                <td><?php echo $i++ . '.'; ?></td>
                <td><?php echo $row->nama; ?></td>
                <td><?php echo $row->jenis_kelamin; ?></td>
                <td><?php echo $row->tanggal_lahir; ?></td>
                <td><?php echo $row->alamat; ?></td>
                <td><?php echo $row->nama_jabatan; ?></td>
				<?php if($_SESSION) : ?>
				<td>
                    <a href="formPegawai.php?id_pegawai=<?php echo $row->id_pegawai; ?> " class="btn btn-warning btn-sm">Edit</a>
                    <a href=" deletePegawai.php?id_pegawai=<?php echo $row->id_pegawai; ?> " class=" btn btn-danger btn-sm" onclick="return confirm('Apakah Anda Yakin akan menghapus data?');">Hapus</a>
                </td>
				 <?php endif; ?>
                
            </tr>

        <?php
        }

        if (!mysqli_num_rows($query)) {
            echo '<tr><td colspan="8" class="text-center">Tidak ada data.</td></tr>';
        }
        ?>

    </tbody>
</table>

<?php include 'footer.php'; ?>